var http = require('http');

var server = http.createServer( function( requ, res){
	res.writeHead( 200,{
		'Content-Type':'html/plain'
	});
	res.write( '<html><i><b>Hello World - 1</b></i></html>' );
	res.end();
});

server.listen( 3000 );

console.log( 'Server running at http://localhost:3000/');