var connect = require( 'connect');
var app = connect();

// middleware function
var helloWorld = function( req, res, next){
	res.setHeader( 'Content-Type', 'text/plain');
	res.end( 'Hello World' );
};
// register with the Connect application
app.use( helloWorld );

app.listen( 3000 );
console.log( 'Server running at port 3000' );