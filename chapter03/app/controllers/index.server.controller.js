exports.render = function( req, res ){
	if( req.session.lastVisit){
		console.log( req.session.lastVisit);
	}
	req.session.lastVisit = new Date();
	
	res.render( 'index', {
		title: 'Hello World'
	} );	
};

exports.render1 = function( req, res ){
	res.send( 'Hello World from render1' );
}