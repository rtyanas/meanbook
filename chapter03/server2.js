var express = require( 'express' );

var hasName = function( req, res, next ){
	if ( req.param( 'name') ){
		next();
		//res.send( 'Hello ' + req.param( 'name') ); // It work where
	} else {
		res.send( 'What is your name?');
	}
};

var sayHello = function( res, req, next ){
	res.send( 'Hello ' + req.param( 'name') ); // It doesn't work here ( 'rep.param is not a function')
};

var app = express();

app.get( '/', hasName, sayHello );

app.listen( 3000 );
console.log( 'Server running at localhost:3000');