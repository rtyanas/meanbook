var express = require( 'express' );
var app = express();

app.use( '/', function (req, res){
	var toSend = 'Hello World';
	toSend += '<br />req.query -> ' + req.query;
	toSend += '<br />req.params -> ' + req.params;
	toSend += '<br />req.body -> ' + req.body; // Why it's undefined?
	//toSend += '<br />req.param(test) -> ' + req.param( test ); // How to use req.param?
	toSend += '<br />req.path -> ' + req.path;
	toSend += '<br />req.hostname -> ' + req.hostname;
	toSend += '<br />req.ip -> ' + req.ip;
	toSend += '<br />req.cookies -> ' + req.cookies; // Why it's undefined?
	toSend += '<br />res.status( 200 ) -> ' + res.status( 200 );
	//res.send( toSend ); // OK
	//res.json( req.params ); //OK
	var testArr = new Array();
	testArr.push( 1 );
	testArr.push( 2 );
	testArr.push( 3 );
	// res.json( testArr ); // How to use that?
	//res.redirect( "http://google.com"); // Can't do this after sending.
});

app.listen( 3000 );
console.log( 'Server1 running at localhost:3000');

module.exports = app;