process.env.NODE_ENV = process.env.NODE_ENV || 'development';
var exprex = require( './config/config.express' );
var app = exprex();

app.listen( 3000 );
console.log( 'Server running at localhost:3000');

module.exports = app; // Why can I remove this, and the system still works?